export default {
  testEnvironment: 'jest-environment-node',
  transform: {},
  testPathIgnorePatterns: ['/node_modules/', '/.nuxt/'],
  setupFilesAfterEnv: ['jest-expect-message']
}
