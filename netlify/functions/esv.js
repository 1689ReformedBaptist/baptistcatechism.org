import fetch from 'node-fetch'

const ENDPOINT = 'https://api.esv.org/v3/passage/html/'

async function handler (event, context) {
  if (!process.env.ESV_API_KEY) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        error: 'Error ESV_API_KEY not set'
      })
    }
  }
  // For some reason, event.queryStringParameters is unreliable on values
  // with spaces when in production.
  const q = new URLSearchParams(event.rawQuery).get('q')

  const params = new URLSearchParams({
    q,
    'include-audio-link': 'false',
    'include-book-titles': 'false',
    'include-footnotes': 'false',
    'include-headings': 'false',
    'include-short-copyright': 'false'
  })
  const queryUrl = `${ENDPOINT}?${params.toString()}`
  console.log('sending request', queryUrl)

  const resp = await fetch(queryUrl, {
    headers: {
      Authorization: `Token ${process.env.ESV_API_KEY}`
    }
  })
  if (resp.status !== 200) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        error: `Error ${resp.status} from ESV API server`
      })
    }
  }

  const data = await resp.json()
  console.log('result from ESV API:', data)
  return {
    statusCode: 200,
    body: JSON.stringify({
      passages: data.passages
    })
  }
}

export {
  handler
}
