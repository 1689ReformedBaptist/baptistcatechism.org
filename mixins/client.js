import delay from 'delay'

export default (pollMillis) => ({
  data () {
    return {
      clientinfo: {
        loaded: false,
        now: null,
        origin: null
      }
    }
  },
  async mounted () {
    // In development mode, we have to give the system a cycle to set the old
    // class first, otherwise it will process mounted() before applying our
    // reactive properties.
    if (process.env.NODE_ENV === 'development') await delay(0)
    this.clientinfo.loaded = true
    this.clientinfo.origin = document.location.origin

    this.clientinfo.now = new Date()
    while (this.pollMillis) {
      await delay(this.pollMillis)
      this.clientinfo.now = new Date()
    }
  }
})
