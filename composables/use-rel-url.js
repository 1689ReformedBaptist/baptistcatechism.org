export default () => {
  const href = useHref()

  return path => new URL(path, href.value).href
}
