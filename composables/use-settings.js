import { reactive, onMounted, watch } from 'vue'

const DEFAULT = {
  translation: 'kjv'
}

export default () => {
  const settings = reactive(DEFAULT)

  onMounted(() => {
    if (typeof localStorage === 'undefined') return
    Object.assign(settings, JSON.parse(localStorage.getItem('settings')))
  })

  watch(settings, v => {
    localStorage.setItem('settings', JSON.stringify(v))
  })

  return settings
}
