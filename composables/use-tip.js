import delay from 'delay'
import tippy from 'tippy.js'
import { ref } from 'vue'

export default () => {
  const tip = ref(null)

  return {
    async flash (o, content) {
      let el = o
      if (o.target) el = o.target

      if (tip.value) tip.value.hide()
      const t = tippy(el, {
        content,
        trigger: 'manual',
        placement: 'top',
        arrow: true
      })
      tip.value = t
      await t.show(275)
      await delay(3000)
      t.hide(250)
    }
  }
}
