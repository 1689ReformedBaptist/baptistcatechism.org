import { computed } from 'vue'
import { useRoute } from 'vue-router'

export default () => {
  const route = useRoute()

  return computed(() => {
    // Reference route.value so that this value is updated every time the
    // route changes.  (The route property cannot give us the origin).
    // eslint-disable-next-line no-unused-expressions
    route.value
    return typeof window === 'undefined'
      ? 'https://example.com'
      : window.location.href
  })
}
