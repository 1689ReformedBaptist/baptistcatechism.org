/* eslint-env jest */
import biblerefs from '@inkylabs/biblerefs'
import fs from 'fs/promises'
import glob from 'glob-promise'
import path from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const ROOT_DIR = path.join(__dirname, '..')

const TRIMMED_PATH = path.join(__dirname, 'trimmed-kjv.json')
const KJV_PATH = path.join(__dirname, 'kjv.json')

function sort (obj) {
  return Object.keys(obj).sort()
}

describe('trimmed-kjv', () => {
  const data = {}
  let kjv, oldData

  beforeAll(async () => {
    kjv = JSON.parse(await fs.readFile(KJV_PATH, 'utf-8'))
    oldData = JSON.parse(await fs.readFile(TRIMMED_PATH, 'utf-8'))

    for (const f of await glob(path.join(ROOT_DIR, 'dist/**/*.html'))) {
      const content = await fs.readFile(f, 'utf-8')
      const bcvrs = biblerefs.flatscan(content, {
        includeChapters: false
      })
      for (const bcv of biblerefs.verses(bcvrs)) {
        const { b, c, v } = bcv
        const ref = biblerefs.format(bcv, { nameStyle: 'id' })
        const kjvb = kjv[b]
        expect(kjvb, `book missing ${ref} in ${f}`).toBeTruthy()
        const kjvc = kjv[b][c - 1]
        expect(kjvc, `chapter missing ${ref} in ${f}`).toBeTruthy()
        const text = kjv[b][c - 1][v - 1]
        expect(text, `verse missing ${ref} in ${f}`).toBeTruthy()
        data[ref] = kjv[b][c - 1][v - 1]
      }
    }

    const bcvrs = biblerefs.mustParse('Gen 1; Psa 67; 83; 110')
    for (const bcv of biblerefs.verses(bcvrs)) {
      const { b, c, v } = bcv
      const ref = biblerefs.format(bcv, { nameStyle: 'id' })
      data[ref] = kjv[b][c - 1][v - 1]
    }

    if (process.env.OVERWRITE_TRIMMED_KJV === 'true') {
      const fp = TRIMMED_PATH
      await fs.writeFile(fp, JSON.stringify(data, sort(data), 2))
      oldData = data
    }
  }, 30 * 60 * 1000)

  it('ref integrity', () => {
    const instruction =
     "\nMake sure you've run generate" +
     '\nRun this command again with OVERWRITE_TRIMMED_KJV=true to fix it'
    const missingRefs = Object.keys(oldData).filter(k => !(k in data))
    const addedRefs = Object.keys(data).filter(k => !(k in oldData))
    expect(missingRefs, `missing refs${instruction}`).toEqual([])
    expect(addedRefs, `added refs${instruction}`).toEqual([])

    const oldStr = JSON.stringify(oldData, sort(oldData))
    const newStr = JSON.stringify(data, sort(data))
    expect(newStr, `verses do not line up${instruction}`).toEqual(oldStr)
  })
})
