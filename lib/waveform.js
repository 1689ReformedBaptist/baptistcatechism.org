/* eslint-env browser */
import seedrandom from 'seedrandom'

// These were gathered with the following command:
// grep -o "fireEvent('[^']*" node_modules/wavesurfer.js/ -rh | sort | uniq
// The documented list may be found here:
// https://wavesurfer-js.org/docs/events.html
const WAVESURFER_EVENTS = [
  'audioprocess',
  'backend-created',
  'canplay',
  'click',
  'contextmenu',
  'dblclick',
  'destroy',
  'deviceError',
  'deviceReady',
  'disable-drag-selection',
  'drawer-created',
  'error',
  'finish',
  'in',
  'interaction',
  'loading',
  'mouseenter',
  'mouseleave',
  'mute',
  'out',
  'pause',
  'play',
  'plugin-added',
  'plugin-destroyed',
  'plugin-initialised',
  'plugins-registered',
  'progress',
  'ready',
  'redraw',
  'region-click',
  'region-contextmenu',
  'region-created',
  'region-dblclick',
  'region-in',
  'region-mouseenter',
  'region-mouseleave',
  'region-out',
  'region-play',
  'region-removed',
  'region-updated',
  'region-update-end',
  'remove',
  'scroll',
  'seek',
  'select',
  'success',
  'update',
  'update-end',
  'volume',
  'waveform-ready',
  'zoom'
]

function getWaveformUrl (url) {
  const re = /https:\/\/(.*?).storage.googleapis.com\/(.*).mp3/
  const match = url.match(re)
  return (
    'https://baptistcatechism-org.storage.googleapis.com/waveforms/' +
    `${match[1]}/${match[2]}.waveform.json`
  )
}

function genFakeWaves (src) {
  // We seed the random number generator so that it's always the same
  // waveform for a given audio file.
  const r = seedrandom(src)
  // (2*r.quick()) - 1  gives us a random number between -1 and 1.
  return Array.apply(null, Array(100)).map(() => 2 * r.quick() - 1)
}

// Fetch and normalize the peaks.
async function fetchPeaks (src) {
  const resp = await fetch(getWaveformUrl(src))
  if (resp.status === 404) {
    // eslint-disable-next-line no-console
    console.log(`No waveform found for ${src}.  Using a random waveform.`)
    return genFakeWaves(src)
  }

  const peaks = await resp.json()
  const max = Math.max(...peaks.data.map(Math.abs))
  return peaks.data.map((d) => d / max)
}

function bucketApiUrl (url) {
  return url.replace(
    /^https:\/\/storage.googleapis.com\/(.*?)\//,
    'https://$1.storage.googleapis.com/'
  )
}

export default class {
  constructor (opts) {
    this.opts = {
      callback: () => {},
      fakewaves: false,
      debugEvents: false,
      ...opts
    }
  }

  async mount (elem) {
    // wavesurfer.js does not support SSR.
    // https://github.com/katspaugh/wavesurfer.js/issues/751
    // https://github.com/katspaugh/wavesurfer.js/issues/2350
    const WaveSurfer = (await import('wavesurfer.js')).default
    const cs = getComputedStyle(elem)

    // Browsers may warn about doing this without some gesture.  However, if we
    // want to display waves even before there is a gesture, we need to create
    // the wavesurfer object, and until wavesurfer.js gives us some alternative,
    // there is no real way around this.
    this.wavesurfer = WaveSurfer.create({
      backend: 'MediaElement',
      container: elem,
      fillParent: true,
      height: elem.clientHeight,
      waveColor: cs.getPropertyValue('--waveform-wave-color') || '#3664a8',
      progressColor:
        cs.getPropertyValue('--waveform-progress-color') || '#81a9e6',
      cursorWidth: 0.5,
      cursorColor: cs.getPropertyValue('--waveform-cursor-color') || '#2d2e30',
      hideScrollbar: true,
      responsive: true,
      barWidth: 4,
      barRadius: 3,
      barMinHeight: 1
    })
    const events = [
      'audioprocess',
      'canplay',
      'finish',
      'ready',
      'waveform-ready',
      'redraw',
      'pause',
      'play',
      'seek'
    ]
    events.forEach((e) => this.wavesurfer.on(e, () => this.opts.callback({
      playing: this.wavesurfer.isPlaying(),
      duration: this.wavesurfer.getDuration(),
      currentTime: this.wavesurfer.getCurrentTime()
    })))

    if (this.opts.debugEvents) {
      WAVESURFER_EVENTS.forEach(e => this.wavesurfer.on(e, () => {
        // eslint-disable-next-line no-console
        console.log(e, this.wavesurfer.isReady)
      }))
    }

    // eslint-disable-next-line no-console
    this.wavesurfer.on('error', console.error)

    // Intentionally do not wait for the redraw fix.
    this.redrawFix(elem)
  }

  // If we mounted the waveloader before the element was added to the dom,
  // then waves won't draw at the right height.  Let's set the height after
  // everything finishes.
  async redrawFix (elem) {
    await new Promise(resolve => {
      if (document.contains(elem)) {
        resolve()
        return
      }
      const observer = new MutationObserver(() => {
        if (document.contains(elem)) {
          resolve(elem)
          observer.disconnect()
        }
      })
      observer.observe(document, { childList: true, subtree: true })
    })

    this.wavesurfer.setHeight(elem.offsetHeight)
    const cs = getComputedStyle(elem)
    this.wavesurfer.setWaveColor(
      cs.getPropertyValue('--waveform-wave-color') || '#3664a8')
    this.wavesurfer.setProgressColor(
      cs.getPropertyValue('--waveform-progress-color') || '#81a9e6')
    this.wavesurfer.setCursorColor(
      cs.getPropertyValue('--waveform-cursor-color') || '#2d2e30')
  }

  async load (src) {
    src = bucketApiUrl(src)
    const peaks = this.opts.fakewaves
      ? genFakeWaves(src)
      : await fetchPeaks(src)
    await this.wavesurfer.load(src, peaks)
  }

  togglePlay (src) {
    this.wavesurfer.playPause()
  }

  setPlaybackRate (speed) {
    this.wavesurfer.setPlaybackRate(speed)
  }

  destroy () {
    this.wavesurfer.destroy()
  }
}
