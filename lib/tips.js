import delay from 'delay'
import tippy, { inlinePositioning } from 'tippy.js'

function setTimestamp (elem) {
  if (!elem.tip) return
  const now = Date.now()
  elem.tip.timestamp = now
  return now
}

async function hide (elem, duration, timestamp) {
  if (!elem.tip) return
  if (timestamp && timestamp !== elem.tip.timestamp) return
  elem.tip.hide()
  await delay(duration)

  if (!elem.tip) return
  elem.tip.destroy()
  delete elem.tip
}

async function flash (elem, text, opts = {}) {
  const hideDuration = 250
  await hide(elem, hideDuration)

  elem.tip = tippy(
    elem,
    Object.assign(
      {
        content: text,
        trigger: 'manual',
        placement: 'top',
        duration: [275, hideDuration],
        arrow: true
      },
      opts
    )
  )
  elem.tip.show()
  const now = setTimestamp(elem)
  await delay(3000)
  await hide(elem, hideDuration, now)
}

function hover (elem, text, opts) {
  return tippy(
    elem,
    Object.assign(
      {
        content: text,
        interactive: true,
        inlinePositioning: true,
        arrow: true,
        plugins: [inlinePositioning]
      },
      opts
    )
  )
}

export default {
  flash,
  hover
}
