import biblerefs from '@inkylabs/biblerefs'
import kjvData from './trimmed-kjv'

function getText (bcv) {
  return kjvData[biblerefs.format(bcv, { nameStyle: 'id' })]
}

function displayPassages (textRef) {
  return biblerefs.flatscan(textRef, { includeBooks: false })
    .map(bcvr => ({
      ref: biblerefs.format(bcvr),
      verses: biblerefs.verses(bcvr)
        .map(bcv => ({
          c: bcv.c,
          v: bcv.v,
          text: getText(bcv)
        }))
    }))
}

export default {
  displayPassages
}
