import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  modules: [
    '@inkylabs/nuxt-content-prerender',
    '@inkylabs/vue-debug-queryparam/nuxt',
    '@inkylabs/vue-konami-utils/nuxt',
    '@kevinmarrec/nuxt-pwa',
    '@nuxt/content',
    '@vue-storefront/nuxt-gtag',
    './modules/contentdump'
  // 'nuxt-purgecss',
  ],
  app: {
    head: {
      htmlAttrs: {
        lang: 'en'
      },
      link: [
        { rel: 'icon', type: 'image/png', href: '/favicon.png' }
      ],
      meta: [
        // name, description, etc. are defined below in the PWA module config
        // section, since that will overwrite of the fields if we set them
        // here.
        { name: 'keywords', content: 'baptist, catechism, doctrine, theology, confession, christianity, jesus, god, religion, spirituality' }
      ]
    }
  },
  build: {
    transpile: [
      'bible-passage-reference-parser/js/en_bcv_parser'
    ]
  },
  components: {
    global: true,
    dirs: ['~/components', '~/layouts']
  },
  contentPrerender: {
    specs: [
      {
        contentDir: '~/content/catechism',
        contentGlob: '*.yml',
        routePath: '/:id($id)',
        componentFile: '~/templates/QuestionPage.vue'
      }
    ]
  },
  css: [
    'vue-material-design-icons/styles.css',
    'tippy.js/dist/tippy.css',
    '@/assets/css/main.scss'
  ],
  // purgeCSS: {
  //   whitelist: [
  //     'infolist',
  //     'infolistset',
  //     'material-design-icon',
  //     'material-design-icon__svg',
  //     'svg'
  //   ]
  // },
  // FUTURE(nuxt>3.0.0-rc.12): Delete this block.
  // For some reason inlineSSRStyles is causing a FOUC on SSR.
  experimental: {
    inlineSSRStyles: false
  },
  gtag: {
    config: {
      id: 'G-NJVXBTY492'
    },
    deferScriptLoad: true
  },
  konami: {
    youtubeAutoplayId: 'umNoOvMOnIw',
    youtubeAutoplayTitle: 'Shai Linne - Only Jesus'
  },
  pwa: {
    meta: {
      name: 'The Baptist Catechism',
      description: 'The Baptist Catechism, also known as Keach’s Catechism or the 1695 Catechism',
      author: 'Silicon Valley Reformed Baptist Church',
      ogImage: 'https://baptistcatechism.org/ogimage.jpg'
    },
    workbox: {
      // Uncomment this to locally test PWA features.
      // It is commented out so that we don't end up relying on a cache during
      // development.
      // enabled: true
    }
  },
  router: {
    // Bootstrap expects .active.
    linkActiveClass: 'active'
  }
})
