const PRESETS = {
  undefined: 100
}

function handleClick (e) {
  window.navigator.vibrate(e.target.dataset.vibratePattern)
}

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.directive('vibrate', {
    mounted (el, binding) {
      el.dataset.vibratePattern = binding.value || PRESETS[binding.arg]
      el.addEventListener('click', handleClick)
    },
    unmounted (el, binding) {
      el.removeEventListener('click', handleClick)
    }
  })
})
