export default {
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      const elem = document.querySelector(to.hash)
      // vue-router does not incorporate scroll-margin-top on its own.
      if (elem) {
        const offset = parseFloat(getComputedStyle(elem).scrollMarginTop)
        return {
          selector: to.hash,
          offset: { top: offset }
        }
      }
    }
    if (savedPosition) return savedPosition
    return { top: 0 }
  }
}
