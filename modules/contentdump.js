import { transformContent } from '@nuxt/content/transformers'
import { defineNuxtModule } from '@nuxt/kit'
import fs from 'fs/promises'
import glob from 'glob-promise'
import path from 'path'

const OUTPUT_NAME = 'baptistcatechism-data.json'

export default defineNuxtModule({
  meta: {
    name: 'contentdump',
    configKey: 'contentdump',
    compatibility: {
      // nuxt: '^3.0.0'
      nuxt: '^3.0.0-rc.12'
    }
  },
  defaults: {},
  hooks: {
    async 'close' (nuxt) {
      const data = {}
      const contentDir = path.join(nuxt.options.rootDir, 'content')
      for (const p of await glob(path.join(contentDir, '**'))) {
        let content
        try {
          content = await fs.readFile(p, 'utf-8')
        } catch (e) {
          if (e.code === 'EISDIR') continue
          throw e
        }
        const rel = path.relative(contentDir, p)
        data[rel] = await transformContent(`test:${rel}`, content)
      }

      const fp = path.join(nuxt.options.generate.dir, OUTPUT_NAME)
      await fs.writeFile(fp, JSON.stringify(data, null, 2))
    }
  }
})
